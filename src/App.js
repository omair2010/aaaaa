import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import "./App.css";
import Navbar from "./components/Navbar";
import Routes from "./routes";
import Footer from "./components/Footer";

function App() {
  const [userConnected, setUserConnected] = useState(false)
  return (
    <Router>
      {userConnected && <Navbar />}
      {true && <Routes />}
      {userConnected && <Footer />}
    </Router>
  );
}

export default App;