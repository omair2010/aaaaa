import React from "react";
import { compose } from "recompose";
import { withStyles, Box } from "@material-ui/core";
import { red, blue, green, yellow } from "@material-ui/core/colors";
import PhoneIcon from "@material-ui/icons/Phone";
import { SvgIcon } from "@material-ui/core";

import logo from "../../images/svg/panorama-24px.svg";
import { HomeStyle } from "../../styles/homeStyle";

const Home = ({ classes }) => {
  const primaryBlue = blue[500];
  const primaryRed = red[500];
  const primaryGreen = green[500];
  const primaryYellow = yellow[500];

  return (
    <div className={classes.root}>
      <Box display="flex" justifyContent="center">
        <Box
          className={classes.box}
          p={1}
          bgcolor={primaryGreen}
          display="flex"
          justifyContent="center"
        >
          <Box alignSelf="flex-end">
            <SvgIcon src={logo} />
            Destinations
          </Box>
        </Box>
        <Box
          className={classes.box}
          p={1}
          bgcolor={primaryRed}
          display="flex"
          justifyContent="center"
        >
          <Box alignSelf="flex-end">Browse services</Box>
        </Box>
        <Box
          className={classes.box}
          p={1}
          bgcolor={primaryYellow}
          display="flex"
          justifyContent="center"
        >
          <Box alignSelf="flex-end">Offers</Box>
        </Box>
        <Box
          className={classes.box}
          p={1}
          bgcolor={primaryBlue}
          display="flex"
          justifyContent="center"
        >
          <Box alignSelf="flex-end">
            <div className={classes.textBox}>Contact us</div>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default compose(withStyles(HomeStyle))(Home);
