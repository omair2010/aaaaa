import React from "react";
import { compose } from "recompose";
import { Grid, withStyles, TextField, Link, Button } from "@material-ui/core";
import { Formik, Form } from "formik";
import Facebook from "@material-ui/icons/Facebook";
import * as Yup from "yup";
import { LoginStyle } from "../../styles/loginStyle";
import logo from "../../images/logo2.png";

const validationShema = () => () =>
Yup.object().shape({
  password: Yup
    .string()
    .min(8, "at least 8 caracters")
    .required("password is required"),
  email: Yup
    .string()
    .email("Invalid email")
    .required("Required")
    .nullable()
});

const Login = ({ classes }) => {
  const icon = (
    <Facebook
      style={{
        borderLeft: "thick solid #fff",
        borderLeftWidth: "1.5px",
        paddingLeft: 10,
        alignSelf: "right"
      }}
    />
  );
  return (
    <Formik
      initialValues={{ email: "", password: "123" }}
      validationShema={validationShema({})}
      onSubmit={() => {}}
    >
      {({ errors, touched, isSubmitting, values }) => (
        
        <Form>
          <Grid
            container
            justify="center"
            alignContent="center"
            alignItems="center"
            className={classes.containerStyle}
          >
            {console.log('touched',touched,'\n errors', errors.email)}
            <Grid item className={classes.cardHeight}>
              <Grid container spacing={2}>
                <Grid item xs={12} className={classes.logoStyle}>
                  <img src={logo} />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name="email"
                    label="Email"
                    type="text"
                    placeholder="example@gmail.com"
                    variant="filled"
                    fullWidth
                    size="small"
                    style={{ backgroundColor: "#fff" }}
                    error={errors.email}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name="password"
                    label="Password"
                    type="password"
                    placeholder=""
                    variant="filled"
                    fullWidth
                    size="small"
                    style={{ backgroundColor: "#fff" }}
                    error={touched.password && !!errors.password}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Link
                    to=""
                    display="flex"
                    style={{ color: "white", fontSize: 14 }}
                  >
                    Forget Password?
                  </Link>
                </Grid>
                <Grid item xs={12}>
                  <hr border="1"></hr>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    style={{ borderRadius: 0, backgroundColor: "#fec23e" }}
                  >
                    Login
                  </Button>
                </Grid>

                <Grid item xs={12} align="center" style={{ color: "white" }}>
                  Or
                </Grid>

                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    className={classes.button}
                    endIcon={icon}
                    style={{ borderRadius: 0, backgroundColor: "#21bdb7" }}
                  >
                    Create Account
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default compose(withStyles(LoginStyle))(Login);
