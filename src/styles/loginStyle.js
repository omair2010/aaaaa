export const LoginStyle = theme => ({
    cardHeight:{
        height: 600,
        width: 350,
        backgroundColor: '#f05561',
        padding : 30
    },
    containerStyle: {
        height: '100vh',
        display: 'flex'
    },
    logoStyle: {
        marginTop: '10%',
        textAlign: 'center',
        marginBottom: 50
    }
})
